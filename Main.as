﻿package {
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.text.*;
	import flash.ui.Keyboard;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.utils.*;
	
	public class Main extends MovieClip {

		var menu:Menu = new Menu();
		var game:Game = new Game();
		var endscreen:EndScreen;
		
		var gameRunning:Boolean = false;
		
		var username:String;
		var shipFlyingIn:Boolean = false;
		var enemiesFlyingIn:Boolean = false;
		var newWave:Boolean = false;
		var score:uint = 0;
		
		var ship:Ship;
		var enemies:Array = new Array();
		var enemyBullets:Array = new Array();
		var health:Array = new Array();

		public function Main() {
			this.addChild(menu);
			menu.newgameButton.addEventListener(MouseEvent.CLICK, startNewGameFirstTime);
			menu.name_txt.restrict = "0-9 A-Z";
			menu.name_txt.maxChars = 11;
			game.notification_txt.alpha = 0;
		}
		
		public function startNewGameFirstTime(e:MouseEvent):void{
			this.removeChild(menu);
			this.startNewGame(e);
			assignKeys();
			game.addEventListener(Event.ENTER_FRAME, gameTick);
		}
		
		/*
		 *	can only be called from menu
		 */
		public function startNewGame(e:MouseEvent):void{
			clearOldValues();
			trace("starting new game by player " + menu.name_txt.text);
			this.username = menu.name_txt.text;
			this.addChild((game = new Game()));
			ship = new Ship(this);
			game.addChild(ship);
			this.shipFlyingIn = true;
			game.notification_txt.alpha = 1;
			this.gameRunning = true;
			stage.focus = game; //fix keyboard issue
			for(var i:uint = 0; i < 3; i++){
				var h:Heart = new Heart();
				h.x = i*50 + 5;
				h.y = 540;
				game.addChild(h);
				health.push(h);
			}
		}
		
		function clearOldValues():void{
			health.splice(0);
			enemyBullets.splice(0);
			enemies.splice(0);
			score = 0;
		}
		
		/*
		 * Only gets called while in a game
		 */
		public function gameTick(e:Event):void{
			if(!gameRunning){
				return;
			}
			if(shipFlyingIn){
				ship.y -= 2;
				if(ship.y == 500){
					shipFlyingIn = false;
					this.startNewRound();
				}
			}
			if(newWave && !enemiesFlyingIn){
				game.notification_txt.alpha = 1;
				spawnEnemies();
				this.newWave = false;
				enemiesFlyingIn = true;
			}
			if(enemiesFlyingIn){
				for(var i:int = 0; i < enemies.length; i++){
					enemies[i].y += 2;
					if(enemies[i].y == 10){
						this.enemiesFlyingIn = false;
						this.newWave = false;
						this.game.notification_txt.alpha = 0;
					}
				}
			}
			ship.tick();
			applyShipMovement();
			applyShipFiring();
			moveEnemyTick();
			shootEnemyTick();
			moveEnemyBulletsTick();
			testEnemyBulletsHitTick();
			drawScore();
			checkNewRound();
		}
		
		
		/*
		 * Can only be called inside a running game
		 */
		public function assignKeys():void{
			stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDownEvent);
			stage.addEventListener(KeyboardEvent.KEY_UP, keyUpEvent);
		}
		
		
		var vx:int = 0;
		var vy:int = 0;
		var shooting:Boolean = false;
		function keyDownEvent(e:KeyboardEvent):void{
			if(!this.gameRunning || this.shipFlyingIn){
				return;
			}
			if(e.keyCode == Keyboard.A || e.keyCode == Keyboard.LEFT){
				vx = -5;
			}
			if(e.keyCode == Keyboard.D || e.keyCode == Keyboard.RIGHT){
				vx = 5;
			}
			if(e.keyCode == Keyboard.W || e.keyCode == Keyboard.UP){
				vy = -5;
			}
			if(e.keyCode == Keyboard.S || e.keyCode == Keyboard.DOWN){
				vy = 5;
			}
			if(e.keyCode == Keyboard.SPACE){
				shooting = true;
			}
		}
		
		function keyUpEvent(e:KeyboardEvent):void{
			if(e.keyCode == Keyboard.A || e.keyCode == Keyboard.LEFT || e.keyCode == Keyboard.D || e.keyCode == Keyboard.RIGHT){
				vx=0;
			}
			if(e.keyCode == Keyboard.W || e.keyCode == Keyboard.UP || e.keyCode == Keyboard.S || e.keyCode == Keyboard.DOWN){
				vy=0;
			}
			if(e.keyCode == Keyboard.SPACE){
				shooting = false;
			}
		}
		
		function spawnEnemies():void{
			for(var i:int = 0; i < 5; i++){
				var enemy:MovieClip = new Enemy(this);
				this.addChild(enemy);
				enemy.x = 120 * i + 120;
				enemy.y = -80;
				enemies.push(enemy);
			}
		}
		
		function applyShipMovement():void{
			if(vx!=0){
				if((ship.x <= 0 && vx<0) || (ship.x >= 720 && vx>0)){
					vx=0;
				}
				ship.x += vx;
				game.background.x -= (vx/8);
			}
			if(vy!=0){
				if((ship.y <= 300 && vy<0) || (ship.y >= 500 && vy>0)){
					vy=0;
				}
				ship.y += vy;
				game.background.y -= (vy/2);
			}
		}
		
		function applyShipFiring():void{
			if(shooting && ship.canShoot()){
				ship.shoot();
			}
		}
		
		function moveEnemyTick():void{
			if(this.enemiesFlyingIn){
				return;
			}
			for(var i:int = 0; i < enemies.length; i++){
				var e:Enemy = enemies[i];
				if(e.isGoingLeft()){
					if(e.x <= 0){
						e.setGoingLeft(false);
						continue;
					}
					e.x -= 3;
				}else{
					if(e.x >= 720){
						e.setGoingLeft(true);
						continue;
					}
					e.x += 3;
				}
			}
		}
		
		function shootEnemyTick():void{
			if(this.enemiesFlyingIn){
				return;
			}
			for(var i:uint = 0; i < enemies.length; i++){
				var e:Enemy = enemies[i];
				if(e.hasShot()){
					continue;
				}
				if(Math.round(Math.random() * 500) > 495){
					trace("enemy " + i + " shooting");
					e.shoot();
				}
			}
		}
		
		function moveEnemyBulletsTick():void{
			for(var i:uint = 0; i < this.enemyBullets.length; i++){
				var b:Bullet = this.enemyBullets[i];
				b.y += 8;
				if(b.y > 600 + b.height){
					for(var j:uint = 0; j < this.enemies.length; j++){
						var e:Enemy = enemies[j];
						if(e.getBullet() == b){
							trace("Bullet removed");
							e.removeBullet();
							break;
						}
					}
					b.parent.removeChild(b);
					this.enemyBullets.splice(i, 1);
				}
				
			}
		}
		
		function testEnemyBulletsHitTick():void{
			for(var i:uint = 0; i < this.enemyBullets.length; i++){
				var b:Bullet = this.enemyBullets[i];
				if(b.hitTestObject(this.ship)){
					b.parent.removeChild(b);
					this.enemyBullets.splice(i, 1);
					if(this.health.length == 1){
						gameOver();
						return;
					}else{
						var h:Heart = this.health[this.health.length - 1];
						h.parent.removeChild(h);
						this.health.splice(this.health.length - 1, 1);
					}
				}
			}
		}
		
		function drawScore():void{
			game.score_txt.text = score.toString();
		}
		
		function gameOver():void{
			this.gameRunning = false;
			this.removeChild(game);
			this.addChild((this.endscreen = new EndScreen()));
			this.endscreen.score_txt.text = this.score.toString();
			this.endscreen.menu_btn.addEventListener(MouseEvent.CLICK, gotoMainMenu);
			this.endscreen.again_btn.addEventListener(MouseEvent.CLICK, this.startNewGame);
		}
		
		function gotoMainMenu(e:MouseEvent):void{
			this.removeChild(this.endscreen);
			this.addChild(menu);
			menu.newgameButton.removeEventListener(MouseEvent.CLICK, startNewGameFirstTime);
			menu.newgameButton.addEventListener(MouseEvent.CLICK, startNewGame);
		}
		
		function checkNewRound():void{
			if(this.enemies.length == 0){
				game.notification_txt.alpha = 1;
				setTimeout(startNewRound, 1000);
			}
		}
		
		function startNewRound():void{
			this.newWave = true;
		}

	}
	
}
