﻿package {
	
	import flash.display.MovieClip;
	
	public class Enemy extends MovieClip {
		
		var m:Main = null;
		var goingLeft:Boolean = true;
		var shot:Boolean = false;
		var bullet:Bullet = null;
		
		public function Enemy(m:Main) {
			this.m = m;
		}
		
		function isGoingLeft():Boolean{
			return this.goingLeft;
		}
		
		function setGoingLeft(value:Boolean):void{
			this.goingLeft = value;
		}
		
		function hasShot():Boolean{
			return this.shot;
		}
		
		function shoot():void{
			var b:Bullet = new Bullet();
			b.x = this.x + 5;
			b.y = this.y + this.height;
			b.rotation = 180;
			m.enemyBullets.push(b);
			m.addChild(b);
			this.shot = true;
			this.bullet = b;
		}
		
		function dead():void{
			m.removeChild(this);
		}
		
		function removeBullet():void{
			this.shot = false;
		}
		
		function getBullet():Bullet{
			return bullet;
		}
	}
	
}
