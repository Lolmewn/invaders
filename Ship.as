﻿package  {
	
	import flash.display.MovieClip;
	
	
	public class Ship extends MovieClip {
		
		var m:Main;
		var bullets:Array = new Array();
		var shootLeft:Boolean = true;
		
		var timeSinceLastShot:uint = 0;
		
		public function Ship(m:Main) {
			this.m = m;
			this.x = 360;
			this.y = 600;
		}
		
		public function tick():void{
			this.timeSinceLastShot++;
			for(var i:int = 0; i < bullets.length; i++){
				var b:Bullet = bullets[i];
				b.y -= 12;
				for(var j:int = 0; j < m.enemies.length; j++){
					var e:Enemy = m.enemies[j];
					if(b.hitTestObject(e)){
						e.dead();
						m.enemies.splice(j, 1);
						m.removeChild(b);
						bullets.splice(i, 1);
						m.score++;
						break;
					}
				}
				if(b.y < -80){
					m.removeChild(b);
					bullets.splice(i, 1);
				}
			}
		}
		
		public function shoot():void{
			var b:Bullet = new Bullet();
			if(shootLeft){
				b.x = this.x + 15;
				this.shootLeft = false;
			}else{
				b.x = this.x + 60;
				this.shootLeft = true;
			}
			b.y = this.y + 14;
			m.addChild(b);
			bullets.push(b);
			this.timeSinceLastShot = 0;
		}
		
		public function canShoot():Boolean{
			if(this.timeSinceLastShot > 10){
				return true;
			}
			return false;
		}
	}
	
}
